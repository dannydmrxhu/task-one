import { Component, Inject, ViewChild, Input, Output, EventEmitter, OnChanges, ElementRef, AfterViewInit } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { debounceTime, map } from 'rxjs/operators';
import { Observable, fromEvent } from 'rxjs';

@Component({
  selector: 'app-invoices-list',
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.css']
})

export class InvoicesListComponent implements AfterViewInit, OnChanges{
  // Declare two arrays of objects that are initialized with the same values
  // one for displaying the invoices depending on the needs of the user and
  // the other for restoring the original values

  invoices = [];
  safeCopy = [];
  @ViewChild('search') input: ElementRef;
  @Input() newInvoice;
  @Input() copyInvoice;
  @Input() removeInvoice;
  @Output() invoiceArray = new EventEmitter();
  @Output() clearItems = new EventEmitter(true);


  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.invoices = (this.storage.get('invoices') !== null) ? this.storage.get('invoices') : [
      {"name": 'Oranges'},
      {"name": 'Apples'},
      {"name": 'Music'},
      {"name": 'Ginger'},
      {"name": 'Notebooks'}
    ];

    this.safeCopy = this.invoices.slice(0);
  }

  ngOnChanges() {

    if(this.newInvoice !== null) {
      this.add(this.newInvoice);
      this.clearItems.emit();
    }

    if(this.copyInvoice !== null) {
      this.copyItem(this.copyInvoice);
      this.clearItems.emit();
    }

    if(this.removeInvoice !== null) {
      this.removeItem(this.removeInvoice);
      this.clearItems.emit();
    }

    this.emitInvoices();
  }

  ngAfterViewInit() {
    const search = fromEvent(this.input.nativeElement, 'keyup')

    const item = search.pipe(map( (event: any) => { return event.target.value; }));

    const debouncedInput = item.pipe(debounceTime(200));

    const sub = debouncedInput.subscribe( val => this.searchItems(val));
  }

  add(newInvoice) {
    this.invoices.unshift(newInvoice);
    this.safeCopy.unshift(newInvoice);
    this.storeInLocalStorage();
  }

  removeItem(item) {
    let pos: number = this.invoices.indexOf(item);
    this.invoices.splice(pos, 1);
    pos = this.safeCopy.indexOf(item);
    this.safeCopy.splice(pos, 1);
    this.storeInLocalStorage();
  }


  copyItem(item) {
    const newItem = item.name.includes('Copy') ? {name: item.name} : {name: item.name + ' (Copy)'};
    this.invoices.unshift(newItem);
    this.safeCopy.unshift(newItem);
    this.storeInLocalStorage();
  }

  storeInLocalStorage() {
    this.storage.set( 'invoices', this.safeCopy );
  }

  emitInvoices() {
    this.invoiceArray.emit(this.invoices);
  }

  searchItems(text: string) {
    // Restore the original array to make sure we don't search items
    // that are already searched or displayed
    this.invoices =  this.safeCopy.slice(0);

    if(text !== '') {

      const numItems: number = this.invoices.length;
      let searched = [];

      for(var i = 0; i < numItems; i++) {
        // Search for the items case insensitive
        if(this.invoices[i].name.toLowerCase().includes(text.toLowerCase())) {
          searched.push(this.invoices[i]);
        }
      }
      // Copy the searched items to the invoices with is used to display them
      this.invoices = searched.splice(0);
    }
    this.emitInvoices();
  }
}
