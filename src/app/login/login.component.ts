import { Component, Inject, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  message: string;
  private user: string = 'admin';
  private pass: string = 'admin';
  @ViewChild('pass') username: ElementRef;
  @ViewChild('user') password: ElementRef;

  constructor(public authService: AuthService, public router: Router, @Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.setMessage();
    if(this.storage.get('user')) {
      this.router.navigate(['/invoices']);
    }
  }

  setMessage() {
    this.message = 'Logged ' + (this.storage.get('user') ? 'in' : 'out');
  }

  login() {
    if(!this.checkUser() || !this.checkPass()) {
          this.message = 'Incorrect username or password!';
          return;
    }
     this.message = 'Authenticating...';

     this.authService.login().subscribe( () => {

     this.storage.set("user", {
       "username": this.user,
       "password": this.pass
     });

     this.setMessage();

     if(this.storage.get('user')) {
        let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/login';

        this.router.navigate([redirect]);
      }
    });
  }

  checkUser() {

    if(this.user === this.username.nativeElement.value) {
      return true;
    }

    return false;
  }

  checkPass() {

    if(this.pass === this.password.nativeElement.value) {
      return true;
    }

    return false;
  }

  logout() {
    this.authService.logout();
    this.setMessage();
  }
}
