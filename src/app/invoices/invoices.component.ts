import { Component } from '@angular/core';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css'],
})


export class InvoicesComponent {

    invoiceArray;
    newInvoice = null;
    copyInvoice = null;
    removeInvoice = null;


    addItem(value: string) {
      let newItem = { "name": value };
      this.newInvoice = newItem;
    }

    getInvoices(invoices) {
      this.invoiceArray = invoices;
    }

    removeItem(item) {
      this.removeInvoice = item;
    }

    copyItem(item) {
      this.copyInvoice = item
    }

    clearItems() {
      this.newInvoice = null;
      this.copyInvoice = null;
      this.removeInvoice = null;
    }
}
