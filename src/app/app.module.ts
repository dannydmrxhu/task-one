import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { StorageServiceModule } from 'angular-webstorage-service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { LoginRoutingModule }      from './login/login-routing.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { AuthService } from './auth.service';
import { InvoicesListComponent } from './invoices-list/invoices-list.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'invoices', component: InvoicesComponent, canActivate: [AuthService]},
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: '**', component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    InvoicesComponent,
    InvoicesListComponent
  ],
  imports: [
    BrowserModule,
    LoginRoutingModule,
    RouterModule.forRoot(
      appRoutes
    ),
    StorageServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
