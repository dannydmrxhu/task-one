import { Injectable, Inject } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                    from '@angular/router';

import { LOCAL_STORAGE, StorageService } from 'angular-webstorage-service';

import { Observable, of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
@Injectable()

export class AuthService implements CanActivate{
  redirectUrl: string;

  constructor(private router: Router, @Inject(LOCAL_STORAGE) private storage: StorageService) { }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      let url: string = state.url;
      return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
      if(this.storage.get('user')) {
        return true;
      }

      this.redirectUrl = url;

      this.router.navigate(['/login']);

      return false;
    }

    login(): Observable<boolean> {
      this.router.navigate(['/invoices']);
      return of(true).pipe(
        delay(1500),
        tap( val => {} )
      );
    }

  logout(): void {
    this.storage.remove('user');
    this.router.navigate(['/login']);
  }
}
